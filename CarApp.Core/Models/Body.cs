﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace CarApp.Core.Models
{
    [Table("Body")]
    public class Body
    {
        [Key]
        public int BodyID { get; set; }
        public string CarBody { get; set; }


    }

}
