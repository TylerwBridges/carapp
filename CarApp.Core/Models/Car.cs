﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CarApp.Core.Models
{
    [Table("Car")]
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        [ForeignKey("BodyID")]
        public virtual Body Body { get; set; }
        public int? BodyID { get; set; }

       
    }
}
