﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarApp.Data.DAL;

namespace CarApp.Services.Interface
{
    public interface ICarService
    {
        CarContext getAll();
    }
}
