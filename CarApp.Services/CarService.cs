﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarApp.Data.DAL;
using CarApp.Data.Interface;
using CarApp.Data.Repositories;
using CarApp.Services.Interface;

namespace CarApp.Services
{
    public class CarService: ICarService
    {
        private ICarRepository carRepo;

        public CarService()
        {
            carRepo = new CarRepository();
        }

        public CarContext getAll()
        {
            return carRepo.Cars;
        }
    }
}
