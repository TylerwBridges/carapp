namespace CarApp.Web.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CarApp.Web.DAL.CarContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CarApp.Web.DAL.CarContext context)
        {

            var bodys = new List<Body>
                {
                    new Body{BodyID = 1, CarBody = "Sedan"},
                    new Body{BodyID = 2, CarBody = "Minivan"},
                    new Body{BodyID = 3, CarBody = "Truck"},

                };

            bodys.ForEach(s => context.Body.Add(s));
            context.SaveChanges();

            var roles = new List<Role>
                {
                    new Role{RoleID = 1, Name = "User"},
                    new Role{RoleID = 2, Name = "Admin"},

                };

            roles.ForEach(s => context.Role.Add(s));
            context.SaveChanges();

            var cars = new List<Car>
                {
                    new Car{Id = 0, Make = "Honda", Model ="Civic", Year = 2019, BodyID = 1},
                    new Car{Id = 1, Make = "Honda", Model ="Accord", Year = 2016, BodyID = 1},
                    new Car{Id = 2, Make = "Honda", Model ="Ridgeland", Year = 2014, BodyID = 3},
                    new Car{Id = 3, Make = "Honda", Model ="Oddessy", Year = 2018, BodyID = 2},
                    new Car{Id = 4, Make = "Toyota", Model ="Corolla", Year = 2017, BodyID = 1},
                    new Car{Id = 5, Make = "Toyota", Model ="Accord", Year = 2016, BodyID = 1},
                    new Car{Id = 6, Make = "Toyota", Model ="Tundra", Year = 2014, BodyID = 3},
                    new Car{Id = 7, Make = "Toyota", Model ="Test Model", Year = 2012, BodyID = 3},

                };


            cars.ForEach(s => context.Car.Add(s));
            context.SaveChanges();






        }
    }
}