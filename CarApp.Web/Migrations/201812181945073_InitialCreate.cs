namespace CarApp.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Body",
                c => new
                    {
                        BodyID = c.Int(nullable: false, identity: true),
                        CarBody = c.String(),
                    })
                .PrimaryKey(t => t.BodyID);
            
            CreateTable(
                "dbo.Car",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Make = c.String(),
                        Model = c.String(),
                        Year = c.Int(nullable: false),
                        BodyID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Body", t => t.BodyID)
                .Index(t => t.BodyID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Car", "BodyID", "dbo.Body");
            DropIndex("dbo.Car", new[] { "BodyID" });
            DropTable("dbo.Role");
            DropTable("dbo.Car");
            DropTable("dbo.Body");
        }
    }
}
