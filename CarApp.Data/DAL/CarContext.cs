﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using CarApp.Core.Models;

namespace CarApp.Data.DAL
{
    public class CarContext: DbContext
    {

        public CarContext(): base("CarContext")
        {
            
        }

        public DbSet<Car> Car { get; set; }
        public DbSet<Body> Body { get; set; }
        public DbSet<Role> Role { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
//            modelBuilder.Entity<Car>().HasRequired(x => x.Id);
//
//            modelBuilder.Entity<Body>().HasRequired(p => p.BodyID);
//            modelBuilder.Entity<Role>().HasRequired(p => p.RoleID);

        }

    }
}
