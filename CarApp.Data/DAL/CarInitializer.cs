﻿using System;
using System.Collections.Generic;
using System.Text;
using CarApp.Core.Models;

namespace CarApp.Data.DAL
{
    public class CarInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CarContext>
    {
        private static void Seed(CarContext context)
        {
            var bodys = new List<Body>
            {
                new Body{BodyID = 0, CarBody = "Sedan"},
                new Body{BodyID = 1, CarBody = "Minivan"},
                new Body{BodyID = 2, CarBody = "Truck"},

            };

            var roles = new List<Role>
            {
                new Role{RoleID = 1, Name = "User"},
                new Role{RoleID = 2, Name = "Admin"},

            };

            var cars = new List<Car>
            {
                new Car{Id = 0, Make = "Honda", Model ="Civic", Year = 2019, BodyID = 0},
                new Car{Id = 1, Make = "Honda", Model ="Accord", Year = 2016, BodyID = 0},
                new Car{Id = 2, Make = "Honda", Model ="Ridgeland", Year = 2014, BodyID = 2},
                new Car{Id = 3, Make = "Honda", Model ="Oddessy", Year = 2018, BodyID = 1},
                new Car{Id = 4, Make = "Toyota", Model ="Corolla", Year = 2017, BodyID = 0},
                new Car{Id = 5, Make = "Toyota", Model ="Accord", Year = 2016, BodyID = 0},
                new Car{Id = 6, Make = "Toyota", Model ="Tundra", Year = 2014, BodyID = 2},
                new Car{Id = 7, Make = "Toyota", Model ="Test Model", Year = 2012, BodyID = 2},
                
            };



            bodys.ForEach(s => context.Body.Add(s));
            context.SaveChanges();

            roles.ForEach(s => context.Role.Add(s));
            context.SaveChanges();

            cars.ForEach(s => context.Car.Add(s));
            context.SaveChanges();





        }
    }
}
