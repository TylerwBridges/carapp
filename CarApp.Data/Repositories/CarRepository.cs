﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarApp.Core.Models;
using CarApp.Data.DAL;
using CarApp.Data.Interface;

namespace CarApp.Data.Repositories
{
    public class CarRepository: ICarRepository
    {
        private ICollection<CarContext> cars;

        public CarRepository()
        {
             cars = new CarContext();
        }

        public ICollection<CarContext> Car
        {
            get { return cars; }

            set { cars = value; }
        }
    }
}
