﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarApp.Core.Models;
using CarApp.Data.DAL;

namespace CarApp.Data.Interface
{
    public interface ICarRepository
    {
        ICollection<CarContext> Cars { get; set;}
    }
}
